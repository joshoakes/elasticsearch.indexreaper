# ElasticSearch.IndexReaper #

ElasticSearch.IndexReaper is a Windows service that periodically deletes expired indices in ElasticSearch according to configured data retention rules.

### How do I get set up? ###

* Clone source
* Open in Visual Studio 2015 (C# 6.0 feature in use)
* Restore NuGet packages
* Build and run tests (optional)
* Configure ElasticSearch.IndexReaper.Service app.config
* Run as a console app or to install as windows service see here => https://github.com/Topshelf/Topshelf

### Configuration ###

There is a custom config section in the app.config file called *purgeConfig*. 

* *elasticsearchApiUrl* is the URL of your ElasticSearch service
* *purgeMode* is either *list* or *purge*. Note that *purge* is destructive and will send an HTTP DELETE request to ElasticSearch. Use *list* for a dry run.
* *pollingIntervalInMinutes* is the length of time to wait in minutes between each run

### Index purge rules ###

* An index name must be in the format **{prefix}-{yyyy.MM.dd}** for example **error-2015.06.23**
* *prefix* is the name of the index
* *retentionInWeeks* is the number of weeks to keep the index around for
* *eventLoggingEnabled* determines whether the service logs to the event log. Note that setting this to true could throw a SecurityException if the event log source doesn't exist and the app is not running under an account with sufficient privileges to access the event log.
* In the example config below, indices called "error" have an 8 week retention policy

** Example Config **

```
#!xml

<purgeConfig
    elasticsearchApiUrl="http://localhost:9200"
    purgeMode="list" 
    pollingIntervalInMinutes="60"
    eventLoggingEnabled="false" >  <!-- purgeMode="list|purge" -->
    
    <indexPurgeRules>
      <add prefix="error" retentionInWeeks="8" />
      <add prefix="warning" retentionInWeeks="4" />
      <add prefix="info" retentionInWeeks="4" />
    </indexPurgeRules>
    
  </purgeConfig>

```