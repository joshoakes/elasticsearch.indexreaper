using System.Collections.Generic;
using ElasticSearch.IndexReaper.Service.Infrastructure;

namespace ElasticSearch.IndexReaper.Tests
{
    public class IndexRuleBuilder
    {
        private readonly List<IndexReaperRule> _rules = new List<IndexReaperRule>();

        public List<IndexReaperRule> Build()
        {
            return _rules;
        }

        public IndexRuleBuilder AddRule(string prefix, int retentionInWeeks)
        {
            _rules.Add(new IndexReaperRule(prefix, retentionInWeeks));
            return this;
        }

        public static IndexRuleBuilder New => new IndexRuleBuilder();
    }
}