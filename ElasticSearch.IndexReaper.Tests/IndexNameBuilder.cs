using System;

namespace ElasticSearch.IndexReaper.Tests
{
    public class IndexNameBuilder
    {
        private static readonly DateTime UtcNow = DateTime.UtcNow;

        public static string Build(int indexAgeInDays, string indexName = "index")
        {
            var dateOfIndex = UtcNow.AddDays(-indexAgeInDays);
            return $"{indexName}-{dateOfIndex:yyyy.MM.dd}";
        }
    }
}