﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ElasticSearch.IndexReaper.Service.Core;
using ElasticSearch.IndexReaper.Service.Infrastructure;
using ElasticSearch.IndexReaper.Service.Logging;
using Moq;
using NUnit.Framework;

namespace ElasticSearch.IndexReaper.Tests
{
    [TestFixture]
    public class IndexReaperServiceTests
    {
        [Test]
        public void IndexReaperService_ListMode_FindOldIndicesInvoked()
        {
            // arrange
	        var config = SetUpMockConfig(IndexPurgeMode.List);
	        var indexReaper = SetUpMockReaper();
			var scheduler = new Mock<IScheduler>();

			var reaperService = new IndexReaperService(
				config.Object, 
				indexReaper.Object,
				scheduler.Object, 
				new Mock<ILogger>().Object);

            // act
            reaperService.Start();
			scheduler.Raise(e => e.Alarm += null, EventArgs.Empty);
            
            // assert
            indexReaper.Verify(x => x.FindOldIndices(), Times.Once);
            indexReaper.Verify(x => x.PurgeOldIndicies(), Times.Never);
        }

        [Test]
        public void IndexReaperService_PurgeMode_PurgeOldIndicesInvoked()
        {
			// arrange
			var config = SetUpMockConfig(IndexPurgeMode.Purge);
			var indexReaper = SetUpMockReaper();
	        var scheduler = new Mock<IScheduler>();

	        var reaperService = new IndexReaperService(
				config.Object,
				indexReaper.Object,
				scheduler.Object,
				new Mock<ILogger>().Object);

			// act
			reaperService.Start();
            scheduler.Raise(e => e.Alarm += null, EventArgs.Empty);

            // assert
            indexReaper.Verify(x => x.PurgeOldIndicies(), Times.Once);
            indexReaper.Verify(x => x.FindOldIndices(), Times.Never);
        }

	    private static Mock<IIndexReaper> SetUpMockReaper()
	    {
			var indexReaper = new Mock<IIndexReaper>();
			indexReaper.Setup(x => x.IndexReaperRules)
					   .Returns(new ReadOnlyCollection<IndexReaperRule>(new List<IndexReaperRule>()));
			return indexReaper;
		}

	    private static Mock<IIndexReaperConfig> SetUpMockConfig(IndexPurgeMode mode)
	    {
			var config = new Mock<IIndexReaperConfig>();
			config.Setup(x => x.PurgeMode).Returns(mode);
			config.Setup(x => x.IndexReaperRules).Returns(new List<IndexReaperRule>());
		    return config;
	    }
    }
}
