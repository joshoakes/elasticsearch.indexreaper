﻿using System.Collections.Generic;
using System.Linq;
using ElasticSearch.IndexReaper.Service.Infrastructure;
using Moq;
using NUnit.Framework;

namespace ElasticSearch.IndexReaper.Tests
{
    [TestFixture]
    public class IndexReaperTests
    {
        private const int OneWeekRetentionPeriod = 1;
        private const int ZeroWeekRetentionPeriod = 0;

        [Test]
        public void CorrectIndicesAreReturnedUponDeletion()
        {
            // arrange
            var indices = new[]
            {
                IndexNameBuilder.Build(2, "error"),
                IndexNameBuilder.Build(8, "error"), // delete
                IndexNameBuilder.Build(12, "error"), 
                IndexNameBuilder.Build(7, "warning"),
                IndexNameBuilder.Build(8, "warning"), // delete
                IndexNameBuilder.Build(10, "debug")
            };

            var reaperRules = IndexRuleBuilder.New
                .AddRule("debug", ZeroWeekRetentionPeriod)
                .AddRule("error", OneWeekRetentionPeriod)
                .AddRule("warning", OneWeekRetentionPeriod)
                .Build();

            var indexReaper = BuildMockIndexReaper(reaperRules, indices);

            // act
            var purgedIndices = indexReaper.PurgeOldIndicies();

            // assert
            Assert.AreEqual(3, purgedIndices.Count);
            Assert.IsTrue(purgedIndices.Contains(IndexNameBuilder.Build(8, "error")));
            Assert.IsTrue(purgedIndices.Contains(IndexNameBuilder.Build(12, "error")));
            Assert.IsTrue(purgedIndices.Contains(IndexNameBuilder.Build(8, "warning")));
        }

        [Test]
        public void FindIndicesToBeDeletedFindsCorrectIndices()
        {
            // arrange
            var indices = new[]
            {
                IndexNameBuilder.Build(12, "warning"),
                IndexNameBuilder.Build(7, "error"),
                IndexNameBuilder.Build(8, "error"), // only this one should be found
            };

            var reaperRules = IndexRuleBuilder.New
                .AddRule("error", OneWeekRetentionPeriod)
                .AddRule("warning", ZeroWeekRetentionPeriod)
                .Build();

            var indexReaper = BuildMockIndexReaper(reaperRules, indices);

            // act
            var oldIndices = indexReaper.FindOldIndices();

            // assert
            Assert.AreEqual(1, oldIndices.Count);
            Assert.IsTrue(oldIndices.Contains(IndexNameBuilder.Build(8, "error")));
        }

        private static Service.Core.IndexReaper BuildMockIndexReaper(
            List<IndexReaperRule> indexRules = null, 
            IEnumerable<string> indicies = null)
        {
            // IIndexReaperConfig
            var reaperConfig = new Mock<IIndexReaperConfig>();
            reaperConfig.Setup(x => x.IndexReaperRules).Returns(indexRules?.ToList());

            // IElasticSearchClient
            var elasticClient = new Mock<IElasticSearchClient>();
            elasticClient.Setup(x => x.AllIndices()).Returns(indicies?.ToList());
            elasticClient.Setup(x => x.DeleteIndex(It.IsAny<string>())).Returns(true);

            return new Service.Core.IndexReaper(indexRules?.ToList(), elasticClient.Object);
        }
    }
}
