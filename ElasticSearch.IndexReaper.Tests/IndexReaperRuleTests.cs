using ElasticSearch.IndexReaper.Service.Infrastructure;
using NUnit.Framework;

namespace ElasticSearch.IndexReaper.Tests
{
	[TestFixture]
	public class IndexReaperRuleTests
	{
		private const int OneWeekRetentionPeriod = 1;
		private const int ZeroWeekRetentionPeriod = 0;

		[Test]
		public void IndexWithZeroWeekRetentionPolicyIsNotMatched()
		{
			var indexName = IndexNameBuilder.Build(1, "index");
			var indexRule = new IndexReaperRule("index", ZeroWeekRetentionPeriod);
			Assert.IsFalse(indexRule.IsMatch(indexName));
		}

		[TestCase(4, true)]
		[TestCase(5, false)]
		public void ProductionIndexMatchesRules(int weeksOfRetention, bool expectedMatch)
		{
			var indexRule = new IndexReaperRule("logstash-applogs", weeksOfRetention);
			var indexName = IndexNameBuilder.Build(30, "logstash-applogs");
			var actualMatch = indexRule.IsMatch(indexName);

			Assert.AreEqual(expectedMatch, actualMatch);
		}

		[TestCase("logstash", true)]
		[TestCase("logstash-eventlog", false)]
		public void IndexRuleMatchesIndexName(string index, bool expectedMatch)
		{
			var indexRule = new IndexReaperRule("logstash", OneWeekRetentionPeriod);
			var indexName = IndexNameBuilder.Build(20, index);
			var actualMatch = indexRule.IsMatch(indexName);

			Assert.AreEqual(expectedMatch, actualMatch);
		}

		[TestCase(0, false)]
		[TestCase(4, false)]
		[TestCase(7, false)] // this shouldn't be deleted because it's exactly 1 week old
		[TestCase(8, true)]  // this should however
		[TestCase(10, true)]
		public void IndexRuleHasExpectedMatchValue(int indexAgeInDays, bool expectedMatchValue)
		{
			var indexName = IndexNameBuilder.Build(indexAgeInDays);
			var indexRule = new IndexReaperRule("index", OneWeekRetentionPeriod);
			var actualMatchValue = indexRule.IsMatch(indexName);
			Assert.AreEqual(expectedMatchValue, actualMatchValue);
		}

		[TestCase(null)]
		[TestCase("")]
		[TestCase("index-2014-05-12")] // hyphens between dates not valid
		[TestCase("index-2014.99.99")] // date invalid values
		[TestCase("not-a-valid-index-name")]
		public void IndexWithInvalidFormatIsNotMatched(string invalidIndexName)
		{
			var indexRule = new IndexReaperRule("index", OneWeekRetentionPeriod);
			Assert.IsFalse(indexRule.IsMatch(invalidIndexName));
		}
	}
}