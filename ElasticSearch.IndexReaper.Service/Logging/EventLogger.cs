using System;
using System.Diagnostics;

namespace ElasticSearch.IndexReaper.Service.Logging
{
    public class EventLogger : ILogger
    {
        private const string LogSource = "ElasticSearch.IndexReaper.Service";
        private const string LogName = "Application";

        public void LogInfo(string message)
        {
            WriteLogEntry(EventLogEntryType.Information, message);
        }

        public void LogWarning(string message)
        {
            WriteLogEntry(EventLogEntryType.Warning, message);
        }

        public void LogError(string message, Exception ex)
        {
            WriteLogEntry(EventLogEntryType.Error, message);
        }

        private static void WriteLogEntry(EventLogEntryType eventType, string message)
        {
            EnsureSourceExists();
            EventLog.WriteEntry(LogSource, message, eventType);
        }

        private static void EnsureSourceExists()
        {
            if (EventLog.SourceExists(LogSource) == false)
                EventLog.CreateEventSource(LogSource, LogName);
        }
    }
}