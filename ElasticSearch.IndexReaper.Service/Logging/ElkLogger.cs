﻿using System;
using AltSource.Logging.Structured;
using AltSource.Logging.Structured.Config;

namespace ElasticSearch.IndexReaper.Service.Logging
{
	public class ElkLogger : ILogger
	{
		private readonly AltSource.Logging.Structured.Interfaces.ILogger _logger;

		public ElkLogger()
		{
			var config = LoggingConfigurationFactory.GetFromConfigSection();
			_logger = StructuredLoggingFactory.GetLogger(config);
		}

		public void LogInfo(string message)
		{
			_logger.Info(message);
		}

		public void LogWarning(string message)
		{
			_logger.Warning(message);
		}

		public void LogError(string message, Exception ex)
		{
			_logger.Error(message, ex);
		}
	}
}
