using System;

namespace ElasticSearch.IndexReaper.Service.Logging
{
    public class ConsoleLogger : ILogger
    {
        public void LogInfo(string message)
        {
            WriteMessage("INFO", message);
        }

        public void LogWarning(string message)
        {
            WriteMessage("WARN", message);
        }

        public void LogError(string message, Exception ex)
        {
            WriteMessage("ERROR", message);
        }

        private static void WriteMessage(string level, string message)
        {
            var formattedLogMessage = $"{DateTime.Now:s} {level} {message}";
            Console.WriteLine(formattedLogMessage);
        }
    }
}