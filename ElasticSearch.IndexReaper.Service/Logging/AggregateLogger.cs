﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ElasticSearch.IndexReaper.Service.Logging
{
	public class AggregateLogger : ILogger
	{
		private readonly List<ILogger> _loggers;

		public AggregateLogger(IEnumerable<ILogger> loggers)
		{
			_loggers = loggers.ToList();
		}

		public void LogInfo(string message)
		{
			_loggers.ForEach(l => l.LogInfo(message));
		}

		public void LogWarning(string message)
		{
			_loggers.ForEach(l => l.LogWarning(message));
		}

		public void LogError(string message, Exception ex)
		{
			_loggers.ForEach(l => l.LogError(message, ex));
		}
	}
}