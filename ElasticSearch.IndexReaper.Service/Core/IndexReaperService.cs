﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ElasticSearch.IndexReaper.Service.Infrastructure;
using ElasticSearch.IndexReaper.Service.Logging;

namespace ElasticSearch.IndexReaper.Service.Core
{
    public class IndexReaperService : IDisposable
    {
        private readonly IIndexReaperConfig _indexReaperConfig;
        private readonly IIndexReaper _indexReaper;
        private readonly IScheduler _scheduler;
        private readonly ILogger _logger;
        private bool _disposed;

        public IndexReaperService(
            IIndexReaperConfig indexReaperConfig, 
            IIndexReaper indexReaper,
            IScheduler scheduler, 
            ILogger logger)
        {
            _indexReaperConfig = indexReaperConfig;
            _indexReaper = indexReaper;
            _scheduler = scheduler;
            _logger = logger;
        }

        public void Start()
        {
            _scheduler.Alarm += SchedulerOnAlarm;
            _scheduler.Start();

	        var formattedRules = new StringBuilder($"IndexReaper started with the following rules{Environment.NewLine}");
			_indexReaper.IndexReaperRules.Aggregate(formattedRules, (b, r) => b.AppendLine(r.ToString()));
            _logger.LogInfo(formattedRules.ToString());
        }

        private void SchedulerOnAlarm(object sender, EventArgs eventArgs)
        {
            try
            {
                _scheduler.Stop();
                ProcessIndicies();
            }
            finally
            {
                if (!_disposed)
                    _scheduler.Start();
            }
        }

        private void ProcessIndicies()
        {
            try
            {
                _logger.LogInfo($"Scanning for old indices in {_indexReaperConfig.PurgeMode} mode");

                var matchedIndices = 
                    _indexReaperConfig.PurgeMode == IndexPurgeMode.Purge ?
                        _indexReaper.PurgeOldIndicies() :
                        _indexReaper.FindOldIndices();

                LogResult(matchedIndices);
            }
            catch (Exception ex)
            {
                _logger.LogError("ElasticSearch index reaper blew up", ex);
            }
        }

        private void LogResult(IReadOnlyCollection<string> matchedIndices)
        {
            var logMessage = new StringBuilder();
            var logMessageAction = _indexReaperConfig.PurgeMode == IndexPurgeMode.Purge ? "deleted" : "found but not deleted";
            logMessage.AppendLine($"{matchedIndices.Count} matching index(es) {logMessageAction}");
            matchedIndices.Aggregate(logMessage, (builder, index) => builder.AppendLine(index));
            _logger.LogInfo(logMessage.ToString());
        }

        public void Stop()
        {
            _scheduler.Stop();
            _logger.LogInfo("IndexReaper stopped");
        }

        public void Dispose()
        {
			_logger.LogInfo("IndexReaper shutting down");

			if (_disposed)
                return;

            _disposed = true;
            (_scheduler as IDisposable)?.Dispose();
        }
    }
}