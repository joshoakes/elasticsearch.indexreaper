﻿using System.Collections.Generic;
using ElasticSearch.IndexReaper.Service.Infrastructure;

namespace ElasticSearch.IndexReaper.Service.Core
{
    public interface IIndexReaper
    {
        List<string> PurgeOldIndicies();
        List<string> FindOldIndices();
	    IReadOnlyList<IndexReaperRule> IndexReaperRules { get; }
    }
}