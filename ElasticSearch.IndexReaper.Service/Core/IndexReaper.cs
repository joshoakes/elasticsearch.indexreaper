﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ElasticSearch.IndexReaper.Service.Infrastructure;

namespace ElasticSearch.IndexReaper.Service.Core
{
    public class IndexReaper : IIndexReaper
    {
        private readonly List<IndexReaperRule> _indexReaperRules;
        private readonly IElasticSearchClient _elasticSearchClient;

	    public IReadOnlyList<IndexReaperRule> IndexReaperRules => new ReadOnlyCollection<IndexReaperRule>(_indexReaperRules);

	    public IndexReaper(List<IndexReaperRule> indexReaperRules, IElasticSearchClient elasticSearchClient)
        {
            _indexReaperRules = indexReaperRules;
            _elasticSearchClient = elasticSearchClient;
        }

        public List<string> PurgeOldIndicies()
        {
            var oldIndicies = FindOldIndices();
            return oldIndicies.Where(index => _elasticSearchClient.DeleteIndex(index)).ToList();
        }

        public List<string> FindOldIndices()
        {
            if (_indexReaperRules == null || _indexReaperRules.Any() == false)
                return new List<string>();

            var allIndices = _elasticSearchClient.AllIndices();

            var oldIndices = from rule in _indexReaperRules
                             from index in allIndices.Where(rule.IsMatch)
                             select index;

            return oldIndices.ToList();
        }
    }
}
