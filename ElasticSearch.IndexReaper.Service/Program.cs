﻿using System;
using ElasticSearch.IndexReaper.Service.Core;
using ElasticSearch.IndexReaper.Service.Infrastructure;
using ElasticSearch.IndexReaper.Service.Logging;
using Topshelf;

namespace ElasticSearch.IndexReaper.Service
{
	internal class Program
    {
	    private static void Main()
        {
            HostFactory.Run(x =>
            {
                x.Service<IndexReaperService>(s =>
                {
                    s.ConstructUsing(BuildIndexReaperService);
                    s.WhenStarted(m => m.Start());
                    s.WhenStopped(m => m.Stop());
                    s.WhenShutdown(m => m.Dispose());
                });
                
                x.RunAsLocalSystem();

                x.SetDescription("ElasticSearch.IndexReaper.Service deletes indexes from elasticsearch");
                x.SetDisplayName("ElasticSearch.IndexReaper.Service");
                x.SetServiceName("ElasticSearchIndexReaper");
            });
        }

        private static IndexReaperService BuildIndexReaperService()
        {
			var logger = new AggregateLogger(new ILogger[] { new ConsoleLogger(), new ElkLogger() });

	        try
	        {
		        var indexReaperConfig = IndexReaperConfig.Config;
		        var indexReaper = new Core.IndexReaper(indexReaperConfig.IndexReaperRules,
			        new ElasticSearchClient(indexReaperConfig.ElasticSearchUrl));
		        var scheduler = new Scheduler(indexReaperConfig.PollingIntervalInMinutes*60*1000);

		        return new IndexReaperService(indexReaperConfig, indexReaper, scheduler, logger);
	        }
	        catch (Exception ex)
	        {
				logger.LogError("Failed to initialize application", ex);
			}

	        return null;
        }
	}
}
