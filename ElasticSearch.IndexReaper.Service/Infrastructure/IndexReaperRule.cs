﻿using System;
using System.Text.RegularExpressions;

namespace ElasticSearch.IndexReaper.Service.Infrastructure
{
    public class IndexReaperRule
    {
        private const int DaysInAWeek = 7;

        public string Prefix { get; }
        public int RetentionInWeeks { get; }

        public IndexReaperRule(string prefix, int retentionInWeeks)
        {
            Prefix = prefix;
            RetentionInWeeks = retentionInWeeks;
        }

        public bool IsMatch(string indexName)
        {
            if (RetentionInWeeks < 1)
                return false;

            if (string.IsNullOrEmpty(indexName))
                return false;

            if (indexName.StartsWith($"{Prefix}-") == false)
                return false;

            return IndexIsOlderThanRetention(indexName);
        }

        private bool IndexIsOlderThanRetention(string indexName)
        {
            DateTime dateOfIndex;
            if (TryParseIndexDate(indexName, out dateOfIndex) == false)
                return false;

            var indexAgeInDays = (DateTime.UtcNow - dateOfIndex).Days;
            var indexAgeInWeeks = (float)indexAgeInDays / DaysInAWeek;

            return indexAgeInWeeks > RetentionInWeeks;
        }

        private bool TryParseIndexDate(string indexName, out DateTime dateOfIndex)
        {
            dateOfIndex = DateTime.MaxValue;
            var datePattern = Prefix + "-" + @"(\d{4}\.\d{2}\.\d{2})";
            var match = Regex.Match(indexName, datePattern);
            if (match.Success && match.Groups.Count > 0)
            {
                return DateTime.TryParse(match.Groups[match.Groups.Count - 1].Value, out dateOfIndex);
            }

            return false;
        }

	    public override string ToString()
	    {
		    return $"Rule: {Prefix} {RetentionInWeeks} weeks";
	    }
    }
}