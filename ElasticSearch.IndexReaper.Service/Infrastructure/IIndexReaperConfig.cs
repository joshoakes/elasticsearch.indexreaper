﻿using System.Collections.Generic;

namespace ElasticSearch.IndexReaper.Service.Infrastructure
{
    public interface IIndexReaperConfig
    { 
        string ElasticSearchUrl { get; }
        IndexPurgeMode PurgeMode { get; }
        List<IndexReaperRule> IndexReaperRules { get; }
        float PollingIntervalInMinutes { get; }
        bool EventLoggingEnabled { get; }
    }
}