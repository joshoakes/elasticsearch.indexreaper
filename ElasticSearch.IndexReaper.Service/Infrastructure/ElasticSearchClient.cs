using System;
using System.Collections.Generic;
using System.Linq;
using Nest;

namespace ElasticSearch.IndexReaper.Service.Infrastructure
{
    public class ElasticSearchClient : IElasticSearchClient
    {
	    private const int RequestTimeoutInMinutes = 2;
	    private readonly ElasticClient _client;

        public ElasticSearchClient(string elasticeSearchApi)
        {
            var settings = new ConnectionSettings(new Uri(elasticeSearchApi));
	        settings.RequestTimeout(TimeSpan.FromMinutes(RequestTimeoutInMinutes));
			_client = new ElasticClient(settings);
        }

        public IList<string> AllIndices()
        {
            var catIndices = _client.CatIndices();
	        return catIndices.Records.Select(r => r.Index).ToList();
        }

        public bool DeleteIndex(string index)
        {
	        var response = _client.DeleteIndex(new DeleteIndexRequest(index));
	        return response.Acknowledged;
        }
    }
}