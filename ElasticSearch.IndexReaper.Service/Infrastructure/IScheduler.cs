using System;

namespace ElasticSearch.IndexReaper.Service.Infrastructure
{
    public interface IScheduler
    {
        event EventHandler Alarm;

        void Start();
        void Stop();
    }
}