﻿namespace ElasticSearch.IndexReaper.Service.Infrastructure
{
    public enum IndexPurgeMode
    {
        /// <summary>
        /// Lists indices that would be deleted if run in Purge mode
        /// </summary>
        List,

        /// <summary>
        /// Deletes expired indices from ElasticSearch
        /// </summary>
        Purge
    }
}