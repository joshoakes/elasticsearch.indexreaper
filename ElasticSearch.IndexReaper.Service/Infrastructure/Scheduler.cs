using System;
using System.Timers;

namespace ElasticSearch.IndexReaper.Service.Infrastructure
{
    public class Scheduler : IScheduler, IDisposable
    {
        private bool _disposed;
        private readonly Timer _timer;
        public event EventHandler Alarm;

        public Scheduler(double milliseconds)
        {
            _timer = new Timer(milliseconds);
            _timer.Elapsed += TimerOnElapsed;
        }
        
        public void Start()
        {
            _timer.Start();
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            var alarm = Alarm;
            alarm?.Invoke(this, EventArgs.Empty);
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;
            _timer.Dispose();
        }
    }
}