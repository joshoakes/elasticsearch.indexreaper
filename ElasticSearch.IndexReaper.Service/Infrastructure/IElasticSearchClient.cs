using System.Collections.Generic;

namespace ElasticSearch.IndexReaper.Service.Infrastructure
{
    public interface IElasticSearchClient
    {
        IList<string> AllIndices();
        bool DeleteIndex(string index);
    }
}